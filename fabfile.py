from fabric.api import *

env.hosts = ['10.40.19.19']
env.key_filename = '~/.ssh/id_rsa'
env.user = 'zdeploy'

project_name = 'humpback-whale-identification'
project_path = f'/data/anhlt11/personal_projects/image-classifications/{project_name}'


def deploy(host='10.40.19.19'):
    """
    Deploy to server development
    """
    print("Update to development....")

    command = "rsync -avh --progress --delete " \
              "--exclude .git --exclude .idea --exclude notebooks --exclude data . " \
              "zdeploy@{0}:{1}".format(host, project_path)
    local(command)
