import math
import torch
import tqdm
from collections import defaultdict

import src.utils as utils


def calculate_metrics(features, labels, is_train=False, **_):
    if is_train:
        return utils.metrics.topK(features, labels, 5)
    return utils.metrics.topK_custom(features, labels, 5)


def annotate_to_images(images, labels, predicts):
    return None


def evaluate_single_epoch(config, model, dataloader,
                          criterion, writer, epoch, postfix_dict):
    model.eval()
    cal_metric_once = config.eval.cal_metric_once

    with torch.no_grad():
        batch_size = config.eval.batch_size
        total_size = len(dataloader.dataset)
        total_step = math.ceil(total_size / batch_size)

        tBar = tqdm.tqdm(enumerate(dataloader), total=total_step)
        loss_list = []
        metric_list_dict = defaultdict(list)
        data_list_dict = defaultdict(list)
        for i, data in tBar:
            images = data['image'].cuda()
            labels = data['label'].cuda()

            outputs, predicts = model(images=images)
            loss = criterion(outputs, labels)
            loss_list.append(loss.item())

            if cal_metric_once:
                for key, value in predicts.items():
                    data_list_dict[key].extend(value.cpu().numpy())
                for key, value in data.items():
                    if key == 'image':
                        continue
                    data_list_dict[key + 's'].extend(value)
            else:
                metric_dict = calculate_metrics(labels=labels, **predicts, **data)
                for key, value in metric_dict.items():
                    metric_list_dict[key].append(value)

            f_epoch = epoch + i / total_step
            desc = '{:5s}, {:.2f} epoch'.format('dev', f_epoch)
            tBar.set_description(desc)
            tBar.set_postfix(**postfix_dict)

            if i % config.train.log_step == 0:
                log_step = int(f_epoch * 10000)
                if writer is not None:
                    annotated_images = annotate_to_images(images, labels, predicts)
                    writer.add_images(annotated_images, split='dev', log_step=log_step)

        log_dict = {'loss': sum(loss_list) / len(loss_list)}
        if cal_metric_once:
            metric_dict = calculate_metrics(**data_list_dict)
            for key, value in metric_dict.items():
                log_dict[key] = value
        else:
            for key, values in metric_list_dict.items():
                log_dict[key] = sum(values) / len(values)

        for key, value in log_dict.items():
            if writer is not None:
                writer.add_scalar('dev/{}'.format(key), value, epoch)
            if key in ['loss', 'score']:
                postfix_dict['dev/{}'.format(key)] = value

        return log_dict['score']


def train_single_epoch(config, model, dataloader,
                       optimizer, criterion, writer, epoch, postfix_dict):
    model.train()

    batch_size = config.train.batch_size
    total_size = len(dataloader.dataset)
    total_step = total_size // batch_size

    log_dict = {}
    tBar = tqdm.tqdm(enumerate(dataloader), total=total_step)
    for i, data in tBar:
        images = data['image'].cuda()
        labels = data['label'].cuda()

        outputs, predicts = model(images)
        loss = criterion(outputs, labels)
        log_dict['loss'] = loss.item()

        metric_dict = calculate_metrics(labels=labels, **predicts)
        log_dict.update(metric_dict)

        loss.backward()
        # update optimizer
        warmup = True
        if warmup and epoch < 3:
            if (i + 1) % 2 == 0:
                optimizer.step()
                optimizer.zero_grad()
        else:
            if config.train.num_grad_acc is None:
                optimizer.step()
                optimizer.zero_grad()
            elif (i + 1) % config.train.num_grad_acc == 0:
                optimizer.step()
                optimizer.zero_grad()

        f_epoch = epoch + i / total_step
        log_dict['lr'] = optimizer.param_groups[0]['lr']
        for key in ['lr', 'loss', 'score']:
            value = log_dict[key]
            postfix_dict['train/{}'.format(key)] = value

        desc = '{:5s}, {:.2f} epoch'.format('train', f_epoch)
        tBar.set_description(desc)
        tBar.set_postfix(**postfix_dict)

        if i % config.train.log_step == 0:
            log_step = int(f_epoch * 10000)
            if writer is not None:
                writer.add_scalars(log_dict, log_step=log_step)


def train(config, model, loaders, optimizer, scheduler, criterion, writer, start_epoch):
    scores = []
    best_score = 0.0
    best_score_avg = 0.0
    postfix_dict = {}
    for epoch in range(start_epoch, config.train.num_epochs):
        # train phase
        train_single_epoch(config, model, loaders['train'],
                           optimizer, criterion, writer, epoch, postfix_dict)

        # val phase
        score = evaluate_single_epoch(config, model, loaders['dev'],
                                      criterion, writer, epoch, postfix_dict)
        scores.append(score)

        # update scheduler
        if config.scheduler.name == 'reduce_lr_on_plateau':
            scheduler.step(score)
        elif config.scheduler.name != 'reduce_lr_on_plateau':
            scheduler.step()

        if epoch % config.train.save_checkpoint_epoch == 0:
            utils.checkpoint.save_checkpoint(config, model, optimizer,
                                             epoch, 0, keep=10)

        # update metrics and log them
        scores = scores[-20:]
        score_avg = sum(scores) / len(scores)
        writer.add_scalar('dev/score_avg', score_avg, epoch)
        if score > best_score:
            best_score = score
            utils.checkpoint.save_checkpoint(config, model, optimizer,
                                             epoch, keep=10, name='best.score')
            utils.checkpoint.copy_last_n_checkpoints(config, 10, 'best.score.{:04d}.pth')
        if score_avg > best_score_avg:
            best_score_avg = score_avg
            utils.checkpoint.save_checkpoint(config, model, optimizer,
                                             epoch, keep=10, name='best.score_avg')
            utils.checkpoint.copy_last_n_checkpoints(config, 10, 'best.score_avg.{:04d}.pth')
    return {'score': best_score, 'score_avg': best_score_avg}
