import click
import numpy as np
import os
import pandas as pd
import pprint
import tqdm

import src.ops as ops
import src.utils as utils
from src.datasets import get_dataloader
from src.losses import get_criterion
from src.models import get_model
from src.optimizers import get_optimizer
from src.schedulers import get_scheduler
from src.transform import get_transform, function
from src.utils.writer import MetricWriter


@click.command()
@click.option("--config_file", required=True, type=str)
@click.option("--experiment", default="", type=str)
def train_whale_classifier(config_file, experiment):
    print('Train humpback whale identification')
    config = utils.config.load_cfg(config_file)
    pprint.PrettyPrinter(indent=2).pprint(config)

    train_dir = config.train.dir
    if experiment != "":
        train_dir = train_dir + "_" + experiment

    for subdir in ["checkpoint", "log", "submission", "image"]:
        os.makedirs(os.path.join(train_dir, subdir), exist_ok=True)

    model = get_model(config)
    optimizer = get_optimizer(config, model.parameters())
    checkpoint = utils.checkpoint.get_initial_checkpoint(config)
    if checkpoint is not None:
        last_epoch, step = utils.checkpoint.load_checkpoint(model, optimizer, checkpoint)
    else:
        last_epoch, step = -1, -1

    print('from checkpoint: {} last epoch:{}'.format(checkpoint, last_epoch))
    scheduler = get_scheduler(config, optimizer, last_epoch)

    preprocess_opt = function.get_preprocess_opt(config, model=model)
    loaders = {split: get_dataloader(config, split, get_transform(config, split, **preprocess_opt))
               for split in ['train', 'val']}
    criterion = get_criterion(config)
    writer = MetricWriter(os.path.join(train_dir, "log"))
    ops.classification.train(config, model, loaders, optimizer, scheduler, criterion, writer, last_epoch)


@click.command()
@click.option("--input_path", default="input.csv", type=str)
@click.option("--output_path", default="./storage/temp/submission.csv", type=str)
@click.option("--threshold", default=0.5, type=float)
def make_submission(input_path, output_path, threshold):
    df_input = utils.submission.ensemble(input_path.split(','))
    df_submission = pd.read_csv('data/sample_submission.csv', index_col='Image')
    df_submission['Id'] = ''

    m = df_input.values
    df_list = []
    thresholds = np.arange(0.5, 0.3, -0.01) if threshold is None else [threshold]
    for threshold in tqdm.tqdm(thresholds):
        df_list.append(utils.submission.fill(df_submission, df_input, threshold))

    sorted_df = list(sorted(df_list, key=lambda x: x[:2]))
    error, new_whale_count, threshold, df_submission = sorted_df[0]
    df_submission.to_csv(output_path)


@click.group()
def cli():
    pass


if __name__ == '__main__':
    cli.add_command(train_whale_classifier)
    cli.add_command(make_submission)
    cli()
