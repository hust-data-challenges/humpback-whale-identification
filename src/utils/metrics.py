import numpy as np
import torch


def apK(actual, predicted, k=5):
    """
    Source: https://github.com/benhamner/Metrics/blob/master/Python/ml_metrics/average_precision.py
    """
    if len(predicted) > k:
        predicted = predicted[:k]
    actual = [actual]

    score = 0.0
    num_hits = 0.0

    for i, p in enumerate(predicted):
        if p in actual and p not in predicted[:i]:
            num_hits += 1.0
            score += num_hits / (i + 1.0)

    if not actual:
        return 0.0

    ret = score / min(len(actual), k)
    return ret


def mapK(actual, predicted, k=5):
    """
    Source: https://github.com/benhamner/Metrics/blob/master/Python/ml_metrics/average_precision.py
    """
    return np.mean([apK(a, p, k) for a, p in zip(actual, predicted)])


def _get_topK(scores, indices, labels, threshold, k):
    # todo

    for index in indices:
        ls, ss = labels[index], scores[index]

    return None


def topK_custom(features, labels, k=5):
    # convert both as np type
    np_features = features
    if isinstance(features, torch.Tensor):
        np_features = features.cpu().numpy()

    np_labels = labels
    if isinstance(labels, torch.Tensor):
        np_labels = labels.cpu().numpy()

    m = np.matmul(np_features, np.transpose(np_features))
    for i in range(np_features.shape[0]):
        m[i, i] = -1000.0

    thresholds = np.arange(0.4, 0.2, -0.02)
    predict_sorted_indices = np.argsort(m, axis=-1)[:, ::-1]

    mapK_list = []
    for threshold in thresholds:
        topKs = []
        for labels, scores, indices in zip(np_labels, m, predict_sorted_indices):
            topK_labels, topK_scores = _get_topK(scores, indices, labels, threshold)
            topKs.append(np.array(topK_labels))
        mapK_list.append((threshold, mapK_list(labels, topKs)))


def topK(features, labels, k=5, is_train=True):
    if is_train:
        _, topK_scores = torch.topk(features, k)

        np_topK_scores = topK_scores
        if isinstance(topK_scores, torch.Tensor):
            np_topK_scores = topK_scores.cpu().numpy()

        np_labels = labels
        if isinstance(labels, torch.Tensor):
            np_labels = labels.cpu().numpy()

        return {"score": mapK(np_labels, np_topK_scores),
                "mapK": mapK(np_labels, np_topK_scores)}

    topK_custom(features, labels, k=k)
