from tensorboardX import SummaryWriter


class MetricWriter:
    def __init__(self, log_dir):
        self.log_dir = log_dir
        self.writer = SummaryWriter(self.log_dir)

    def add_image(self, image, split='temp', name="test_1", log_step=None):
        self.writer.add_image(f'{split}/{name}', image, log_step)

    def add_images(self, images, split='train', log_step=None):
        for idx, image in enumerate(images):
            self.writer.add_image(f'{split}/image_{idx}', image, log_step)

    def add_scalar(self, split, key, value, log_step):
        self.writer.add_scalar(f'{split}/{key}', value, log_step)

    def add_scalars(self, log_dict, split='train', log_step=None):
        for key, value in log_dict:
            self.writer.add_scalar(f'{split}/{key}', value, log_step)
