import numpy as np
import pandas as pd


def get_topK(scores, id_list, threshold, k=5):
    used = set()
    ret_ids = []

    indices = np.argsort(scores)[::-1]

    for index in indices:
        l = id_list[index]
        s = scores[index]
        # if l in used:
        #     continue

        if 'new_whale' not in used and s < threshold:
            used.add('new_whale')
            ret_ids.append('new_whale')

        used.add(l)
        ret_ids.append(l)
        if len(ret_ids) >= k:
            break

    return ret_ids[:k]


def ensemble(input_paths):
    df_list = [pd.read_csv(input_path, index_col='Image')
               for input_path in input_paths]
    df = sum(df_list) / len(df_list)
    return df


def fill(df_submission, df_input, threshold):
    df_submission = df_submission.copy()
    assert len(df_submission[df_submission['Id'] == '']) == len(df_submission)

    m = df_input.values

    id_list = df_input.columns
    new_whale_count = 0
    for row_index, scores in enumerate(m):
        top5 = get_topK(scores, id_list, threshold=threshold, k=5)
        if top5[0] == 'new_whale':
            new_whale_count += 1
        df_submission.iat[row_index, 0] = ' '.join(top5)

    assert len(df_submission[df_submission['Id'] == '']) == 0
    return (abs(new_whale_count - 2197),
            new_whale_count,
            threshold,
            df_submission)
