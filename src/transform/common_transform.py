import numpy as np
from albumentations import Resize, HorizontalFlip
from imgaug import augmenters as iaa

from .function import to_norm_bgr


def common_transform(split, size, mean, std, input_space, input_range,
                     flip=False, align=True, align_p=1.0, **_):
    print('[common_transform] align:', align)
    print('[common_transform] align_p:', align_p)
    print('[common_transform] flip:', flip)
    resize = Resize(height=size, width=size, always_apply=True)
    flip_aug = HorizontalFlip(always_apply=True)

    seq = iaa.Sequential([
        iaa.Sometimes(0.5, iaa.AverageBlur(k=(3, 3))),
        iaa.Sometimes(0.5, iaa.MotionBlur(k=(3, 5))),
        iaa.Add((-10, 10), per_channel=0.5),
        iaa.Multiply((0.9, 1.1), per_channel=0.5),
        iaa.Sometimes(0.5, iaa.Affine(
            scale={'x': (0.9, 1.1), 'y': (0.9, 1.1)},
            translate_percent={'x': (-0.05, 0.05), 'y': (-0.05, 0.05)},
            shear=(-10, 10),
            rotate=(-10, 10)
        )),
        iaa.Sometimes(0.5, iaa.Grayscale(alpha=(0.8, 1.0))),
    ], random_order=True)

    def transform(image):
        image = image.astype(np.float32)
        H, W, _ = image.shape
        if split == 'train':
            seq_det = seq.to_deterministic()
            assert image.shape[0] > 0 and image.shape[1] > 0, '{}'.format(image.shape)
            image = seq_det.augment_images([image])[0]

        image = resize(image=image)['image']
        if flip:
            image = flip_aug(image=image)['image']

        image = to_norm_bgr(image, mean, std, input_space, input_range)
        image = np.transpose(image, [2, 0, 1])
        image = image.astype(np.float32)
        return image

    return transform
