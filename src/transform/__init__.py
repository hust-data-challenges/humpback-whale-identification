from . import function
from .algin_transform import *
from .common_transform import *


def get_transform(config, split, params=None, **kwargs):
    f = globals().get(config.transform.name)

    if params is not None:
        return f(split, **config.transform.params, **params, **kwargs)
    else:
        return f(split, **config.transform.params, **kwargs)
