import numpy as np


def get_preprocess_opt(config, **kwargs):
    opt = {}
    model = kwargs.get("model", None)
    if config.model.params.pretrained and model is not None:
        opt['mean'] = model.mean
        opt['std'] = model.std
        opt['input_range'] = model.input_range
        opt['input_space'] = model.input_space
    else:
        opt['mean'] = [0.5, 0.5, 0.5]
        opt['std'] = [0.5, 0.5, 0.5]
        opt['input_range'] = [0, 1]
        opt['input_space'] = 'RGB'

    return opt


def to_norm_bgr(image, mean, std, input_space, input_range):
    assert isinstance(image, (np.ndarray,))
    if input_space == 'rgb':
        image = image[..., ::-1]

    if max(input_range) != 255:
        image = image / 255 * max(input_range)

    image -= mean
    image /= std
    return image


def from_norm_bgr(image, mean, std, input_space, input_range):
    assert isinstance(image, (np.ndarray,))
    image *= std
    image += mean

    if max(input_range) != 255:
        image = image / max(input_range) * 255

    if input_space == 'rgb':
        image = image[..., ::-1]

    return image
