from .common import *


def get_scheduler(config, optimizer, last_epoch):
    func = globals().get(config.scheduler.name)
    print()
    return func(optimizer, last_epoch, **config.scheduler.params)
