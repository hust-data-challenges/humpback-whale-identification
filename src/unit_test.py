import unittest

import src.utils as utils
from src.datasets import get_dataloader
from src.transform import get_transform
from src.transform.function import get_preprocess_opt

config = utils.config.load_cfg("../configs/densenet121.1st.yml")
config.data.dir = "../data/"


class Test(unittest.TestCase):
    def test_dataloader(self):
        preprocess_opt = get_preprocess_opt(config)
        loaders = get_dataloader(config, "train",
                                 get_transform(config, "train", **preprocess_opt))
