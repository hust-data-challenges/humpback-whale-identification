import cv2
import os
import pandas as pd
from abc import ABC
from torch.utils.data.dataset import Dataset


class WhaleClassification(Dataset, ABC):
    def __init__(self, dataset_dir, split, transform=None, version=-1, train_csv='train.csv', **_):
        self.split = split
        self.transform = transform
        self.dataset_dir = dataset_dir
        self.train_csv = os.path.join(dataset_dir, train_csv)
        if version != -1:
            self.train_csv = f"{self.train_csv}.{version}"

        if split == 'test':
            self.images_dir = os.path.join(dataset_dir, 'test')
        else:
            self.images_dir = os.path.join(dataset_dir, 'train')

        self.df_examples = self.load_examples()
        self.size = len(self.df_examples)

    def load_examples(self):
        df = pd.read_csv(self.train_csv)
        df['filepath'] = df['Image'].apply(lambda item: os.path.join(self.images_dir, item))
        return df

    def __getitem__(self, index):
        example = self.df_examples.iloc[index % self.size]
        image = cv2.imread(example['filepath'])
        return {'image': image, 'label': example['Id'], 'key': example['Image']}

    def __len__(self):
        return self.size
