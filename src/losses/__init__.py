from .arcface import *
from .common import *
from .focal_loss import *


def get_criterion(config):
    f = globals().get(config.loss.name)
    return f(**config.loss.params)
