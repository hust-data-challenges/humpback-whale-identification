import pretrainedmodels
import torch.nn as nn

from .custom_model import CustomModel


def get_transferred_model(model_name, backbone='resnet18', num_outputs=None, pretrained=True, **_):
    model = CustomModel()
    return model


def get_pretrained_models(model_name='resnet18', num_outputs=None, pretrained=True, **_):
    pretrained = 'imagenet' if pretrained else None
    model = pretrainedmodels.__dict__[model_name](num_classes=1000, pretrained=pretrained)

    if 'dpn' in model_name:
        in_channels = model.last_linear.in_channels
        model.last_linear = nn.Conv2d(in_channels, num_outputs, kernel_size=1, bias=True)
    else:
        model.avgpool = nn.AdaptiveAvgPool2d(1)
        in_features = model.last_linear.in_features
        model.last_linear = nn.Linear(in_features, num_outputs)

    return model


def get_model(config, type_model="default", **kwargs):
    print('model name:', config.model.name)
    if type_model == "default":
        f = lambda **params: get_pretrained_models(config.model.name, **params)
    else:
        f = lambda **params: get_pretrained_models(config.model.name, config.model.base_model, **params)

    if config.model.params is None:
        return f(**kwargs)
    else:
        return f(**config.model.params, **kwargs)
