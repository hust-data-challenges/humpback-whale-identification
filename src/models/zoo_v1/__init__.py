from .dpn import *
from .inceptionV4 import *
from .nasnet import *
from .senet import *
from .xception import *


def get_zoo_model(model_name, num_classes=1000, in_channels=3, pretrained=True):
    if model_name == 'xception':
        base_model = xception(num_classes=num_classes, pretrained=pretrained)
    elif model_name == 'inceptionv4':
        base_model = inceptionv4(num_classes=num_classes, pretrained='imagenet')
    elif model_name == 'dpn68':
        base_model = dpn68(num_classes=num_classes, pretrained=pretrained)
    elif model_name == 'dpn92':
        base_model = dpn92(num_classes=num_classes, pretrained=pretrained)
    elif model_name == "dpn98":
        base_model = dpn98(num_classes=num_classes, pretrained=pretrained)
    elif model_name == "dpn107":
        base_model = dpn107(num_classes=num_classes, pretrained=pretrained)
    elif model_name == "dpn131":
        base_model = dpn131(num_classes=num_classes, pretrained=pretrained)
    elif model_name == 'seresnext50':
        pretrained = 'imagenet' if pretrained else None
        base_model = se_resnext50_32x4d(num_classes=num_classes, inchannels=in_channels, pretrained=pretrained)
    elif model_name == 'seresnext101':
        pretrained = 'imagenet' if pretrained else None
        base_model = se_resnext101_32x4d(num_classes=num_classes, inchannels=in_channels, pretrained=pretrained)
    elif model_name == 'seresnet101':
        pretrained = 'imagenet' if pretrained else None
        base_model = se_resnet101(num_classes=num_classes, pretrained=pretrained, inchannels=in_channels)
    elif model_name == 'senet154':
        pretrained = 'imagenet' if pretrained else None
        base_model = senet154(num_classes=num_classes, pretrained=pretrained, inchannels=in_channels)
    elif model_name == "seresnet152":
        pretrained = 'imagenet' if pretrained else None
        base_model = se_resnet152(num_classes=num_classes, pretrained=pretrained)
    elif model_name == 'nasnet':
        pretrained = 'imagenet' if pretrained else None
        base_model = nasnetalarge(num_classes=num_classes, pretrained=pretrained)
    else:
        assert False, "{} is error".format(model_name)

    return base_model
