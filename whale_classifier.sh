# shellcheck disable=SC2034
entry_path=$(readlink -f "$0")
project_home=$(dirname "$entry_path")

job_name="whale_classification"
log_dir="/data/anhlt11/logs/kaggle/image_classification/"
env_python="/data/khoint3/miniconda/envs/anhlt11-gpu/bin/python"

pid_file="$log_dir/${job_name}_$1.pid"
log_file="$log_dir/${job_name}_$1.log"

[ ! -d "$log_dir" ] && mkdir -p "$log_dir"
[ "x$2" = "x" ] && echo "Empty config file" && exit 1

config_file="${project_home}/$2"
[ ! -e "$config_file" ] && echo "File $config_file not found. Please restart your job" && exit 1

experiment_prefix=$3
[ "x$experiment_prefix" = "x" ] && experiment_prefix=""

print_info() {
  echo "log_file: $log_file"
  echo "pid_file: $pid_file"
  echo "config_file: $config_file"
}

# shellcheck disable=SC2120
training_service() {
  echo "Train model as job name $job_name"
  args="--config_file $config_file --experiment $experiment_prefix"
  run_command="$env_python -m src.main train-whale-classifier $args"
  $run_command
#  run_command="$env_python -m src.main train-whale-classifier $args &"
#  $run_command 1>>"$log_file" 2>>"$log_file" &
#  /bin/echo -n $! >"$pid_file"
}

inference_service() {
  echo "Predict model as job name $job_name"
  /bin/echo -n $! >"$pid_file"
}

submission_service() {
  echo "Submit result predicted from classifier as job name $job_name"
  /bin/echo -n $! >"$pid_file"
}

export CUDA_VISIBLE_DEVICES=0,1,2,3
case "$1" in
train)
  print_info
  if [ -e "$pid_file" ]; then
    echo "training job is running"
    echo
    exit 1
  fi
  echo
  training_service
  echo
  ;;
predict)
  print_info
  if [ -e "$pid_file" ]; then
    echo "inference job is running"
    echo
    exit 1
  fi
  echo
  inference_service
  echo
  ;;
submit)
  print_info
  if [ -e "$pid_file" ]; then
    echo "submission job is running"
    echo
    exit 1
  fi
  echo
  submission_service
  echo
  ;;
stop)
  print_info
  if [ -e "$pid_file" ]; then
    echo "job already stopped"
    echo
    exit 1
  fi
  # shellcheck disable=SC2046
  kill -9 $(cat "$pid_file")
  rm "$pid_file" -f
  echo "Stopped!"
  echo
  ;;
restart)
  print_info
  $entry_path stop
  echo "Waiting..."
  sleep 5
  $entry_path start
  ;;
*)
  echo
  exit 1
  ;;
esac
